//Ejercicio 2
/*
2.1 Inserta dinamicamente en un html un div vacio con javascript. */
let createDiv = document.createElement("div");
document.body.appendChild(createDiv);

/* 2.2 Inserta dinamicamente en un html un div que contenga una p con javascript. */
let createDivP = document.createElement("div");
let createP = document.createElement("p");
createDivP.appendChild(createP);
document.body.appendChild(createDivP);

/* 2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.*/
let createDivSixP = document.createElement("div");
document.body.appendChild(createDivSixP);
for (let i = 0; i < 6; i++) {
let sixP = document.createElement("p");
createDivSixP.appendChild(sixP);
}

/* 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'. */
let createPText = document.createElement("p");
createPText.textContent = "Soy dinámico! :D";
document.body.appendChild(createPText);

/* 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'. */
let h2Text = document.querySelector(".fn-insert-here")
h2Text.textContent = "Wubba Lubba dub dub Morty!";

/* 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter']; */
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
let createUl = document.createElement("ul");
for (const app of apps) {
    let createLi = document.createElement("li");
    createLi.textContent = app;
    createUl.appendChild(createLi);
}
document.body.appendChild(createUl);

/* 2.7 Elimina todos los nodos que tengan la clase .fn-remove-me */
let deleteNodes = document.querySelectorAll(".fn-remove-me"); 
for (const element of deleteNodes) {
    element.remove();
}

/* 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
Recuerda que no solo puedes insertar elementos con .appendChild. */
let createSecondP = document.createElement("p");
selectorDivs = document.querySelectorAll("div");
createSecondP.textContent = "Voy en medio!";
document.body.insertBefore(createSecondP, selectorDivs[1]);

/* 2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here */
let insertClass = document.querySelectorAll(".fn-insert-here");
for (const singleClass of insertClass) {
    let createPInside = document.createElement("p");
    createPInside.textContent = "Voy para dentro!"
    singleClass.appendChild(createPInside);
}